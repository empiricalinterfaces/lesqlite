//
//  SQLite.h
//  SQLite
//
//  Created by Aaron Zinman on 10/1/15.
//  Copyright © 2015 Empirical Interfaces Inc. All rights reserved.
//

//! Project version number for SQLite.
FOUNDATION_EXPORT double SQLiteVersionNumber;

//! Project version string for SQLite.
FOUNDATION_EXPORT const unsigned char SQLiteVersionString[];

#import <SQLite/sqlite3.h>